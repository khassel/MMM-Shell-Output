// MagicMirror²
// Module: MMM-Shell-Output

const { exec } = require("node:child_process");
const NodeHelper = require("node_helper");

module.exports = NodeHelper.create({
  socketNotificationReceived(notification, payload) {
    const replaceSpecial = (text) => {
      return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
    }

    if (notification === "READ_SHELL_OUTPUT") {
      var self = this;
      exec(payload, (err, stdout, stderr) => {
        if (err) {
          self.sendSocketNotification("SHELL_OUTPUT", {
            payload,
            content: replaceSpecial(stderr),
          });
        } else {
          self.sendSocketNotification("SHELL_OUTPUT", {
            payload,
            content: replaceSpecial(stdout),
          });
        }
      });
    }
  },
});
