// MagicMirror²
// Module: MMM-Shell-Output

Module.register("MMM-Shell-Output", {
  defaults: {
    header: "SHELL-OUTPUT",
    command: "ls -la $HOME",
    updateIntervalSeconds: 30,
    cssStyleName: "default",
  },

  getStyles: () => {
    return ["MMM-Shell-Output.css"];
  },

  start() {
    Log.info(`Starting module: ${this.name}`);
    this.content = "";
    this.readFile();
    setInterval(() => {
      this.readFile();
    }, this.config.updateIntervalSeconds * 1000);
  },

  readFile() {
    this.sendSocketNotification("READ_SHELL_OUTPUT", this.config.command);
  },

  getDom() {
    const wrapper = document.createElement("div");

    const header = document.createElement("header");
    header.innerHTML = this.config.header;
    wrapper.appendChild(header);

    const preformatted = document.createElement("shelloutput");
    preformatted.id = this.config.cssStyleName;
    preformatted.innerHTML = this.content;

    wrapper.appendChild(preformatted);
    return wrapper;
  },

  socketNotificationReceived(notification, payload) {
    if (notification === "SHELL_OUTPUT") {
      this.content = payload.content;
      this.updateDom();
    }
  },
});
