# Shell Output for MagicMirror²

This is a module for the [MagicMirror²-Project](https://github.com/MagicMirrorOrg/MagicMirror/).

I was looking for a module displaying the output of commands executed on the host, e.g. list the content of the home directory with `ls -la`.

The command is added in the config file of MagicMirror², so you are very flexible. You can e.g.

- list directory contents with `ls`
- view file contents with `cat`
- display log files

but also more sophisticated things as displaying the pods in your kubernetes cluster. The only restriction is that the command is available on the host, so if you e.g. want to list your pods with `kubectl get pod` `kubectl` must be installed.

## Examples

### Files in a directory

![](img/ls_la.JPG)

### Kubernetes Cluster

![](img/kubectl.JPG)

## Installation

Installation is very simple, just clone this repository into your modules directory then add the module to your config.

```shell
cd ~/MagicMirror/modules
git clone https://gitlab.com/khassel/MMM-Shell-Output.git
```

## Using the module

At the moment this module is quite basic. To use it, add the following configuration block to the modules array in the `config/config.js` file:

```js
let config = {
  modules: [
    {
      module: "MMM-Shell-Output",
      position: "top_right",
      config: {
        header: "SHELL-OUTPUT",
        command: "ls -la $HOME",
        updateIntervalSeconds: 30,
        cssStyleName: "default",
      },
    },
  ],
};
```

You can update the above properties according to your taste:

- `header`: The Title displayed over the output
- `command`: The command executed
- `updateIntervalSeconds`: The frequency the command is executed in seconds
- `cssStyleName`: The name of the css style
- `position`: Module position

More changes are possible editing the [css-File](MMM-Shell-Output.css), e.g. you can change the number of the displayed lines by changing the property `max-height: 20em; /* 20em means 20 lines */`. Best way to do this is to copy the content in your `~/MagicMirror/css/custom.css` file and to make the changes there.

You can use this module with several instances in your mirror. If you need different css styles you can use the `cssStyleName` property to define different styles, e.g. `cssStyleName: "module1"` would expect a style named `shelloutput#module1` in `custom.css`:

```css
shelloutput#module1 {
  display: block;
  unicode-bidi: embed;
  font-family: monospace;
  white-space: pre;
  text-align: left;
  padding: 0;
  margin: 0;
  max-width: 600px;
  max-height: 40em; /* 40em means 40 lines */
  overflow: hidden;
  text-overflow: ellipsis;
  position: relative;
  font-size: 60%;
  line-height: 100%;
  border-bottom: solid 1px #666;
}
```
